#!/bin/sh

plugin="free-cccam-lines-installer scripts"
scripts_path="/usr/script"
url="https://gitlab.com/eliesat/free/-/raw/main/cccam-scripts"

if [ ! -d "$scripts_path" ]; then
    mkdir -p $scripts_path
fi

declare -A files=(
["$scripts_path/free-cccam-centrum-new.sh"]="$url/free-cccam-centrum-new.sh"
["$scripts_path/free-cccam-zoom1.sh"]="$url/free-cccam-zoom1.sh"
["$scripts_path/free-cccam-zoom2.sh"]="$url/free-cccam-zoom2.sh"
["$scripts_path/free-cccam-server.sh"]="$url/free-cccam-server.sh"
)

echo "> Downloading $plugin, please wait ..."
sleep 3
for file in "${!files[@]}"
do
    wget --show-progress -qO "$file" "${files[$file]}"
    sleep 1
done

echo "> $plugin installed successfully"
sleep 1
echo "> uploaded by eliesat"
sleep 3



