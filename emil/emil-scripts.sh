#!/bin/sh
#https://github.com/emil237/ajpanel/raw/main/script.tar.gz

#check wget
opkg install wget >/dev/null 2>&1
sleep 3

#download scripts
echo "> downloading emil scripts please wait ..."
sleep 3

cd /tmp
set -e
rm -rf *scripts.tar.gz* >/dev/null
wget --show-progress -q --no-check-certificate https://gitlab.com/eliesat/free/-/raw/main/emil/emil-scripts.tar.gz
tar -xzf emil-scripts.tar.gz -C /
rm -rf *emil-scripts.tar.gz* >/dev/null 2>&1
echo
set +e
echo ''
chmod 755 /usr/script/*.sh

#openpli image
if grep -qs -i "openpli" /etc/issue; then

if [ ! -d /etc/cron/scripts ]; then
     mkdir /etc/cron/scripts
echo "> scripts cron folder created successfully"
else
echo "> scripts cron folder already exists"
fi

cp -rf /usr/script/* /etc/cron/scripts >/dev/null 2>&1
chmod 755 /etc/cron/scripts/*.sh

fi

echo "> scripts are installed in default path successfully"
sleep 3
echo "> uploaded by Eliesat enjoy ..."
sleep 3

exit 0
