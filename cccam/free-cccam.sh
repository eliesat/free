#!/bin/sh

echo "> removing emus please wait..."
sleep 3s
rmfiles=(
OSCam* Ncam* powercam ncam oscam
)

for file in "${rmfiles[@]}"; do
cd /usr/bin/ >/dev/null 2>&1
killall -9 ${file} >/dev/null 2>&1
cd /usr/bin/cam/ >/dev/null 2>&1
killall -9 ${file} >/dev/null 2>&1
done

sleep 3s

rmfiles=(
OSCam* oscam* ncam* Ncam* powercam* oscamicam* CCcam*
)

for file in "${rmfiles[@]}"; do
rm -rf /usr/camd/${file} >/dev/null 2>&1
rm -rf /usr/emu/${file} >/dev/null 2>&1
rm -rf /usr/scr/${file} >/dev/null 2>&1
rm -rf /usr/scr/cam/${file} >/dev/null 2>&1
rm -rf /usr/softcams/${file} >/dev/null 2>&1
rm -rf /var/emu/${file} >/dev/null 2>&1
rm -rf /var/scr/${file} >/dev/null 2>&1
rm -rf /usr/bin/${file} >/dev/null 2>&1
rm -rf /usr/bin/cam/${file} >/dev/null 2>&1
done

sleep 3s

m -rf /etc/ncam* > /dev/null 2>&1
rm -rf /usr/camscript/Ncam* > /dev/null 2>&1
rm -rf /usr/script/*cam.sh > /dev/null 2>&1
rm -rf /usr/script/*em.sh > /dev/null 2>&1
rm -rf /usr/camscript/*cam.sh > /dev/null 2>&1
rm -rf /usr/emu_scripts/EGcam* > /dev/null 2>&1
rm -rf /etc/init.d/softcam* > /dev/null 2>&1
rm -rf /usr/emu/start/*emu > /dev/null 2>&1
rm -rf /usr/emuscript/*em.sh > /dev/null 2>&1
rm -rf /usr/LTCAM/*ncam.sh > /dev/null 2>&1
rm -rf /usr/script/cam/*Oscam.sh > /dev/null 2>&1
rm -rf /usr/script/cam/*OSCam.sh > /dev/null 2>&1
rm -rf /usr/script/cam/*OScam.sh > /dev/null 2>&1
rm -rf /usr/script/cam/*Oscam.sh > /dev/null 2>&1
rm -rf /usr/script/cam/*ncam.sh > /dev/null 2>&1
rm -rf /usr/script/cam/*Ncam.sh > /dev/null 2>&1
rm -rf /usr/script/cam/*NCam.sh > /dev/null 2>&1
rm -rf /usr/script/*emu > /dev/null 2>&1
rm -rf /etc/*emu.emu > /dev/null 2>&1
rm -rf /usr/script/*cam.sh > /dev/null 2>&1
rm -rf /etc/cam.d/*Oscam.sh > /dev/null 2>&1
rm -rf /etc/cam.d/*OSCam.sh > /dev/null 2>&1
rm -rf /etc/cam.d/*OScam.sh > /dev/null 2>&1
rm -rf /etc/cam.d/*Oscam.sh > /dev/null 2>&1
rm -rf /etc/cam.d/*ncam.sh > /dev/null 2>&1
rm -rf /etc/cam.d/*Ncam.sh > /dev/null 2>&1
rm -rf /etc/cam.d/*NCam.sh > /dev/null 2>&1

sleep 3s

opkg remove --force-depends enigma2-plugin-softcams-gosatplusv2-oscam > /dev/null 2>&1
opkg remove --force-depends enigma2-plugin-softcams-oscam > /dev/null 2>&1
opkg remove --force-depends enigma2-plugin-softcams-oscamicam > /dev/null 2>&1
opkg remove --force-depends enigma2-plugin-softcams-powercam-oscam > /dev/null 2>&1
opkg remove --force-depends enigma2-plugin-softcams-supcam-oscam > /dev/null 2>&1
opkg remove --force-depends enigma2-plugin-softcams-ncam > /dev/null 2>&1
opkg remove --force-depends enigma2-plugin-softcams-revcamv2-ncam > /dev/null 2>&1
opkg remove --force-depends enigma2-plugin-softcams-supcam-ncam > /dev/null 2>&1
opkg remove --force-depends enigma2-plugin-softcams-powercam-ncam > /dev/null 2>&1
opkg remove --force-depends enigma2-plugin-softcams-gosatplusv2-ncam > /dev/null 2>&1
opkg remove --force-depends enigma2-plugin-softcams-gosatplus2 > /dev/null 2>&1
opkg remove --force-depends enigma2-plugin-softcams-powercam > /dev/null 2>&1
opkg remove --force-depends enigma2-plugin-softcams-revcamv2 > /dev/null 2>&1
opkg remove --force-depends enigma2-softcams-cccam-images > /dev/null 2>&1
opkg remove --force-depends enigma2-softcams-cccam > /dev/null 2>&1
sleep 3s

echo "> removing config files please wait..."
sleep 3s

rm -rf /etc/tuxbox/config > /dev/null 2>&1
rm -rf /etc/tuxbox/gosatplus > /dev/null 2>&1
rm -rf /etc/tuxbox/powercam > /dev/null 2>&1
rm -rf /etc/tuxbox/ultracam > /dev/null 2>&1
rm -rf /etc/CCcam.cfg > /dev/null 2>&1
rm -rf /usr/keys/* > /dev/null 2>&1
sleep 3s

# Configuration
pack="oscam"
version="11812-emu-r802"
package="enigma2-plugin-softcams-oscam"
#determine package manager
if [ "$package_manager" == "apt" ]; then
    ipk="$pack-$version.deb"
    install_command="dpkg -i --force-overwrite"
    uninstall_command="apt-get purge --auto-remove -y"
else
    ipk="$pack-$version.ipk"
    install_command="opkg install --force-reinstall"
    uninstall_command="opkg remove --force-depends"
fi
url="https://gitlab.com/eliesat/free/-/raw/main/cccam/$ipk"
temp_dir="/tmp"

# Determine package manager
if command -v dpkg &> /dev/null; then
    package_manager="apt"
    status_file="/var/lib/dpkg/status"
else
    package_manager="opkg"
    status_file="/var/lib/opkg/status"
fi

# Functions
print_message() {
    echo "[$(date +'%Y-%m-%d %H:%M:%S')] $1"
}

cleanup() {
    
    [ -d "/CONTROL" ] && rm -rf /CONTROL >/dev/null 2>&1
    rm -rf /control /postinst /preinst /prerm /postrm /tmp/*.ipk /tmp/*.tar.gz >/dev/null 2>&1
    print_message "Activate the emu and restart enigma2"
}

check_and_install_package() {
    if grep -q "$package" "$status_file"; then
        print_message "Removing existing $package package, please wait..."
        $uninstall_command $package
    fi

    print_message "Downloading $pack-$version, please wait..."
    wget -q --show-progress $url -P "$temp_dir"
    if [ $? -ne 0 ]; then
        print_message "Failed to download $pack-$version from $url"
        exit 1
    fi

    print_message "Installing $pack-$version, please wait..."
    $install_command "$temp_dir/$ipk"
    if [ $? -eq 0 ]; then
        print_message "$pack-$version installed successfully."
    else
        print_message "Installation failed."
        exit 1
    fi
}

# Main
trap cleanup EXIT
check_and_install_package

#download cccam config file
rm -rf /etc/tuxbox/config/CCcam.cfg
wget --show-progress -qO /etc/tuxbox/config/CCcam.cfg "https://gitlab.com/eliesat/free/-/raw/main/cccam/CCcam.cfg"
download=$?
echo ''
if [ $download -eq 0 ]; then
echo "> installation of cccam config file finished"
sleep 3
fi




