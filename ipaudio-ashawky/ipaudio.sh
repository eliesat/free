#!/bin/sh

#check python version
python=$(python -c "import platform; print(platform.python_version())")
sleep 1;
case $python in 
2.7.18)
echo ""
;;
3.8.5)
echo ""
;;
3.9.7|3.9.9)
echo ""
;;
3.10.4)
echo ""
;;
3.11.0|3.11.1|3.11.2|3.11.3|3.11.4|3.11.5|3.11.6)
echo ""
;;
3.12.*)
echo ""
;;
*)
echo "> your image python version: $python is not supported"
sleep 3
exit 1
;;
esac

# Remove unnecessary files and folders
[ -d "/CONTROL" ] && rm -r /CONTROL >/dev/null 2>&1
rm -rf /control /postinst /preinst /prerm /postrm /tmp/*.ipk /tmp/*.tar.gz >/dev/null 2>&1



#config
plugin=ipaudio
version=ashawky
url=https://gitlab.com/eliesat/free/-/raw/main/ipaudio-ashawky/ipaudio.tar.gz
package=/var/volatile/tmp/$plugin-$version.tar.gz

#download & install
echo "> Downloading $plugin-$version package  please wait ..."
sleep 3s

wget --show-progress -qO $package --no-check-certificate $url
download=$?
if [ $download -eq 0 ]; then
rm -rf /etc/enigma2/IPAudioPro.json >/dev/null 2>&1
rm -rf /usr/lib/enigma2/python/Plugins/Extensions/IPaudioPro/assets/picons >/dev/null 2>&1
fi
tar -xzf $package -C /
extract=$?
rm -rf $package >/dev/null 2>&1

echo ''
if [ $extract -eq 0 ]; then
echo "> $plugin-$version package installed successfully"
echo "> Uploaded By ElieSat"
sleep 3s

else

echo "> $plugin-$version package installation failed"
sleep 3s
fi
